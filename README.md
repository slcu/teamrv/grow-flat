# grow_flat

This is a simple 2D web simulation of Marchantia growth, forked from flat (groupFN). It is meant as a simple, phenomenological demonstration of the variety of morphologies that result from changing growth parameters.

Our little digital liverwort grows by adding new circular elements at regular intervals, which then go on to grow in size for some time. Older elements are darker, and will eventually disappear (die). Occasionally, branches split into two daughter branches, which then go on to grow independently. When branches overlap, one of them stops growing.

The sliders control the growth parameters and the noise on the branching angle.

Try it out [online](https://slcu.gitlab.io/teamrv/grow-flat).

08.12.2021
