var cvs, sliderL, sliderW, sliderA;
// starting position 
var xi, yi;
// initial angle
var ai;
//array with positions of cells
let cellarray=[]; 
//keeping track of time
var counter; 
//keep track of growth cycles
var growthcycle;
//keep track of when to branch
var branching;

function updateCellAge(){
    this.age+=1;
}
function growCell(ww){
    this.rad+=ww; //W should become growth rate
}

//Object, type cell: the unit which is drawn. 
//Don't include functions into object: copying into the array becomes more difficult
function Cell(id, age, posx, posy, rad, angle, mom){ 
    this.id=id;
    this.age=age;
    this.posx=posx; //current position of the element
    this.posy=posy; //current position of the element
    this.rad=rad; //current radius of the element
    this.angle=angle; //angle of this element with its mother
    this.mom=mom; //ID of mother
    
    //this.child1=child1;
   // this.child2=child2;//if this element is a branching point, it has 2 children
    //this.updateCellAge=updateCellAge;
    //this.growCell=growCell;

}

//first function to be called
function init()
{
    //apparently we don't need to apply document.getElementById for canvas when the ID is canvas
    cvs = canvas.getContext('2d'); 
	setInterval(draw, 10);

    //retrieve slider objects from html body
    sliderL = document.getElementById("sliderL");
    sliderW = document.getElementById("sliderW");
    sliderA = document.getElementById("sliderA");
    
    initArray();
}

//upon pressing the button
function restart()
{
    cellarray.length=0;
    initArray();
}

function initArray()
{
    // initial position and orientation
    xi = 0.5 * canvas.width;
    yi = 0.5 * canvas.height;
    ai = -0.5 * Math.PI;

    //initialise the array with two initial cells
    cellarray.push(new Cell(0,0,xi-0.5*10, yi, 10, -1.0* Math.PI, -1));    
    cellarray.push(new Cell(1,0,xi+0.5*10, yi, 10, 0, -1));

    //we use this counter to know when to update the cell positions
    counter=0;
    growthcycle=0;
    branch=0;
}

//draws one circle
function drawCircle(ctx, x, y, radius, color, stroke) {
    ctx.beginPath()
    ctx.arc(x, y, radius, 0, 2 * Math.PI, false)
    ctx.fillStyle = color;
    ctx.strokeStyle = stroke;
    ctx.fill()
    ctx.stroke();
  }
    

function drawCells()
{
    //go through array of cell objects; draw circles for each of them
    cellarray.forEach(function(item, index) {
        if(!('gemma' in item)){
            r=0;
            g=Math.max(100,Math.trunc(255-0.25*item.age));
            b=0;
            a=Math.max(Math.min(255,Math.trunc(875-item.age)),0);
            cellcolor="#"+(r).toString(16).padStart(2,'0')+(g).toString(16).padStart(2,'0')+(b).toString(16).padStart(2,'0')+(a).toString(16).padStart(2,'0'); //"#008800";
            stroke=cellcolor;
        }else{
            r=80;
            g=Math.max(100,Math.trunc(250-0.25*item.age));
            b=0;
            a=Math.max(Math.min(255,Math.trunc(875-item.age)),0);
            cellcolor="#"+(r).toString(16).padStart(2,'0')+(g).toString(16).padStart(2,'0')+(b).toString(16).padStart(2,'0')+(a).toString(16).padStart(2,'0'); //"#008800";
            stroke="#"+(89).toString(16).padStart(2,'0')+(60).toString(16).padStart(2,'0')+(31).toString(16).padStart(2,'0')+(a).toString(16).padStart(2,'0'); //"#008800"; 
        }
       drawCircle(cvs,item.posx, item.posy, item.rad, cellcolor,stroke);
       //console.log(item.age)
    });
    
    //draw a blinker
    // if (!(growthcycle%10)){
    //     drawCircle(cvs,50, 50, 25, cellcolor);
    //    // console.log(growthcycle);
    // }

    //drawCircle(cvs, 0.5 * canvas.width, 0.5 * canvas.height, 5, "#880000");
}

function updateCellPositions(ww, ll, aa, bb){
    //grow and branch the cells --> update objects in array, and add new ones (first into temp array)
    //loop over elements, growing all those that are young enough; add extra cells and even branches
    
    elnum=cellarray.length; //current nr of elements in array
    newcells=0;
    branching=0;
    let temp=[];
    let swap=[];
    //loop
    cellarray.forEach(function(cell, index) {

        //admin
        cell.age+=1;
        //console.log("growthcycle "+growthcycle+" index "+ index+" id "+cell.id);
        
        //grow this element until it has a child element
        if(cell.age < 400 ){ //!('child' in cell)
            if (!('gemma' in cell)){
                cell.rad+=ww/((cell.age/30)+1);
            }else if (cell.rad<15){
                cell.rad+=ww/((cell.age/30)+1);
            }
            
        }
        //calculate new position of this cell based on growth of parental cells and own cell
        //note that parents are updated first
        if(cell.mom<0){
            nuposx=0.5 * canvas.width+(-0.5+1.0*cell.id)*cell.rad;
            nuposy=0.5 * canvas.height;
            cell.posx=nuposx;
            cell.posy=nuposy;
        }else if (!( 'gemma'  in cell)){
            let mommy=cellarray[cell.mom];
            if(mommy.age<100 || cell.age<100){
                nuposx=mommy.posx+0.5*(mommy.rad+cell.rad)*Math.cos(cell.angle);
                nuposy=mommy.posy+0.5*(mommy.rad+cell.rad)*Math.sin(cell.angle); 
                cell.posx=nuposx;
                cell.posy=nuposy;
            }
        }else if ('gemma' in cell){
            let mommy=cellarray[cell.mom];
            if(mommy.age<100){
                cell.posx=mommy.posx;
                cell.posy=mommy.posy; 
            }
        }

        //this is a tip cell; extend if it is the right time
        //can we branch?
        if(!('child' in cell) && !(growthcycle%ll) && growthcycle>0 && !(branch%bb)){
            //     //calculate new position and new angle for two new daughter cell
                nuang=cell.angle+(0.5*aa+(noise*(1.0-2*Math.random()) ) );
                nuposx=cell.posx+0.5*(cell.rad+10)*Math.cos(nuang);
                nuposy=cell.posy+0.5*(cell.rad+10)*Math.sin(nuang);
                temp.push(new Cell(elnum,0,nuposx, nuposy, 10, nuang, cell.id));  
                elnum+=1;
                nuang=cell.angle-(0.5*aa+(noise*(1.0-2*Math.random()) ) );
                nuposx=cell.posx+0.5*(cell.rad+10)*Math.cos(nuang);
                nuposy=cell.posy+0.5*(cell.rad+10)*Math.sin(nuang);
                temp.push(new Cell(elnum,0,nuposx, nuposy, 10, nuang, cell.id));  
                elnum+=1;
                //add gemma cup
                if(cell.id>1){
                    temp.push(new Cell(elnum,cell.age,cell.posx, cell.posy, 2, 0, cell.id));
                    temp[temp.length - 1].gemma=1;
                    temp[temp.length - 1].child=1;
                    elnum+=1;  
                }
                cell.child=2;
                branching+=1;
                
        }
        else if(!('child' in cell) && !(growthcycle%ll) && growthcycle>0){ //don't branch, just extend
            //calculate new position for daughter cell
             //     console.log("yay");
             angle=cell.angle+(noise*(1.0-2*Math.random()) );
             nuposx=cell.posx+0.5*(cell.rad+10)*Math.cos(angle);
             nuposy=cell.posy+0.5*(cell.rad+10)*Math.sin(angle);
             temp.push(new Cell(elnum,0,nuposx, nuposy, 10, angle, cell.id));  
             cell.child=1;  
             elnum+=1;
             branching+=1;             
        }
        else if(!('child' in cell) && cell.mom>=0){ //check if this cell overlaps with another
            cellarray.forEach(function(cell2, index2) {
                if (!('child' in cell2) && cell.mom!=cell2.mom){
                    dist=Math.hypot(cell.posx-cell2.posx, cell.posy-cell2.posy);
                    if(dist<=cell.rad+cell2.rad){
                        //pick random cell to stop growing 
                        pick=Math.random();
                        if(pick>0.5){
                            cell2.child=1;
                            console.log('child 1');
                            //cell2.age=400; //switch ids to draw stopped one underneath? a.findIndex(x => x.prop2 ==="yutu");
                            if(cellarray.findIndex(x=> x.id === cell2.id )>cellarray.findIndex(x=> x.id === cell.id )){   
                                swap.push([cellarray.findIndex(x=> x.id === cell2.id ),cellarray.findIndex(x=> x.id === cell.id )]);
                            }
                        }else{
                            cell.child=1;
                            console.log('child 2');
                            if(cellarray.findIndex(x=> x.id === cell.id )>cellarray.findIndex(x=> x.id === cell2.id )){
                                swap.push([cellarray.findIndex(x=> x.id === cell.id ),cellarray.findIndex(x=> x.id === cell2.id )]);
                            }
                        }
                        
                    }
                }
            });
        }
        
    });
    growthcycle+=1;
    //temp.forEach(function(item, index) {
    //    cellarray.push(copy(item));
    //});
    cellarray.push(...temp);
    temp.length=0;
    if(branching){
        branch+=1;
    }

    //swap the element that stopped growing with the element that will continue
    swap.forEach(function(tuple) {
        tempvar=cellarray[tuple[0]]
        cellarray[tuple[0]]=cellarray[tuple[1]];
        cellarray[tuple[1]]=tempvar;
    });
}


function drawBranch(i, x0, y0, len, l, w, t, a)
{
    //i: nr of pattern repetitions still to do
    //x0,y0:  starting pos of branch
    //len: inital branch length
    //l: branch length
    //w: branch width
    //t:this branch's angle
    //a: general branching angle in radians

    var PI_2 = 0.5 * Math.PI;
    var x1 = x0 + len * Math.cos(t);
    var y1 = y0 + len * Math.sin(t);
    var dx = -w * Math.sin(t);
    var dy =  w * Math.cos(t);
    //console.log(`at ${x1}, ${y1}`);
	cvs.beginPath();
	cvs.fillStyle = "#008800";
	cvs.strokeStyle = "yellow";
    cvs.moveTo(x0+dx, y0+dy);
    cvs.lineTo(x1+dx, y1+dy);
    cvs.arc(x1, y1, w, t+PI_2, t-PI_2, true);
    cvs.lineTo(x1-dx, y1-dy);
    cvs.lineTo(x0-dx, y0-dy);
    cvs.lineTo(x0+dx, y0+dy);
    cvs.closePath();
	cvs.fill();
    cvs.stroke();
    
    if ( i > 0 )
    {
        var nl = 0.75 * l;
        drawBranch(i-1, x1+0.5*dx, y1+0.5*dy, nl, nl, 0.5*w, t+0.5*a, a);
        drawBranch(i-1, x1-0.5*dx, y1-0.5*dy, nl, nl, 0.5*w, t-0.5*a, a);
    }
}

function draw()
{
    //retrieve slider values
    W = sliderW.value*0.01; //growth rate of cells
    L = sliderL.value; //extension rate of cells
    A = sliderA.value * Math.PI / 180; //branch angle
    B = sliderB.value;
    noise = sliderN.value*0.1;
    //update cell positions
    if(counter==10){
        updateCellPositions(W,L,A,B);
        //console.log(counter);
        counter=0;
    }

	cvs.clearRect(0, 0, canvas.width, canvas.height);
    //drawBranch(3, xi, yi, 0.5*L, L, W, ai, A);
    //drawBranch(3, xi, yi, 0.5*L, L, W, ai+Math.PI, A);
    drawCells();
    counter+=1;
}

